# Configuration file for the Sphinx documentation builder.
#
# For the full list of built-in configuration values, see the documentation:
# https://www.sphinx-doc.org/en/master/usage/configuration.html

# -- Project information -----------------------------------------------------
# https://www.sphinx-doc.org/en/master/usage/configuration.html#project-information
import os
import sys

import rapidpe_rift_pipe

sys.path.append(os.path.abspath("./_ext"))

project = 'RapidPE/RIFT Pipeline'
copyright = '2022, Caitlin Rose, Vinaya Valsan, Daniel Wysocki, Sinead Walsh, Patrick Brady'
author = 'Caitlin Rose, Vinaya Valsan, Daniel Wysocki, Sinead Walsh, Patrick Brady'
release = rapidpe_rift_pipe.__version__

# -- General configuration ---------------------------------------------------
# https://www.sphinx-doc.org/en/master/usage/configuration.html#general-configuration

extensions = [
    'ini',
    'sphinx.ext.todo',
    'sphinx_tabs.tabs',
]

templates_path = ['_templates']
exclude_patterns = ['_build', 'Thumbs.db', '.DS_Store']



# -- Options for HTML output -------------------------------------------------
# https://www.sphinx-doc.org/en/master/usage/configuration.html#options-for-html-output

html_theme = 'alabaster'
html_static_path = ['_static']
