`create_submit_dag_one_event.py ` is set to run these post scripts automatically.

If need to run independently, use `./makeplots.sh`

# Extrinsic Plots

- save high weight(ratio of marginalized likelihood) intrinsic points in a text file 
`python3 convert_result_to_txt.py /home/vinaya.valsan/rapidPE/RapidPE_RIFT_Developments/RapidPE-RIFT_forks_DistCoord/rapidpe-rift-submission-scripts/output/mc_eta_test4/inj_33478/ 0.99`

- to plot skymap:
`python3 plot_skymap.py /home/vinaya.valsan/rapidPE/RapidPE_RIFT_Developments/RapidPE-RIFT_forks_DistCoord/rapidpe-rift-submission-scripts/output/mc_eta_test4/inj_33478/ 0.99`


# Intrinsic plots

- to plot intrinsic grids and intrinsic posterior
`python3 plot_intrinsic_posterior.py /home/vinaya.valsan/rapidPE/RapidPE_RIFT_Developments/RapidPE-RIFT_forks_DistCoord/rapidpe-rift-submission-scripts/output/mc_eta_test4/inj_33478/ mchirp_eta`
