#!/bin/bash
makeplots() {
    ligolw_no_ilwdchar $1/results/*
    convert_result_to_txt.py $1 $2 $4
    mkdir -p $1/summary_plots
    if [ ! -z $4 ]
    then
    mkdir -p $4/summary_plots
    fi
    plot_skymap.py $1 $2 $4
    plot_intrinsic_posterior.py $1 $3 $4
    create_summarypage.py $1 $4 
}

input_dir=/home/vinaya.valsan/rapidPE/RapidPE_RIFT_Developments/RapidPE-RIFT_forks_sample_MDC2/test_m1m2_coordinate/output/mass1mass2_coordinate_test1/bbh_injection5/inj_3
output_dir=
coordinates=mchirp_eta
weight_frac=0.9
if [ ! -z $output_dir ]
then
makeplots $input_dir $weight_frac $coordinates $output_dir &> $output_dir/summary_plots/summary_plots.out
else
makeplots $input_dir $weight_frac $coordinates &> $input_dir/summary_plots/summary_plots.out
fi
